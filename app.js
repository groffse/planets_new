/**
 * Created by Dewald Els on 2019-08-28
 * @description: Demonstration code for building a very basic api in Node with Express.
 * @author: Dewald Els
 * @copyright Noroff Accelerate
 */
const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
// Custom Imports
const { db } = require('./db');
// Import our Model Classes.
const { Auth } = require('./models/auth');
const { Planet } = require('./models/planet');
// Store paths that do NOT require an api_key
const publicPath = [
    '/api/v1/auth/generate-key'
];
const cors = require('cors');

app.use(cors());

// Instantiate our models.
const planet = new Planet(db.getPool());
const auth   = new Auth(db.getPool());

// Express Middleware.
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

// Authentication Middleware
// Note the use of async in the function.
app.use(async (req, resp, next)=>{   
    if (publicPath.includes(req.originalUrl)) {
        return next();
    }
    const authResult = await auth.authenticate(req, resp);
    if (authResult === true) {
        next();
    } else {
        resp.status(401).json(authResult);
    }
});

// Planets Endpoints
app.get('/api/v1/planets', (req, resp) => planet.getPlanets(req, resp));
app.post('/api/v1/planets/add', (req, resp) => planet.addPlanet(req, resp));
app.delete('/api/v1/planets/delete', (req, resp) => planet.deletePlanet(req, resp));
app.put('/api/v1/planets/update', (req, resp) => planet.updatePlanet(req, resp)); // You can also use a PATCH instead of PUT.

// Auth Endpoints.
app.post('/api/v1/auth/generate-key', (req, resp) => auth.generateApiKey(req, resp));

// Open a Port and start listening for requests.
app.listen(port, ()=>{
    // Successfully started the Server.
    console.log(`Planets API Server started on port ${port}`);
});