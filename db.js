const { Pool } = require('pg');

class DB {

    /**
     *  Instantiate the Pool object.
     * This is our connection to the database. The DB class should handle this.
    */
    constructor() {
        this.config = {
            user: 'fbyjngsvnduvtr',
            host: 'ec2-54-221-212-126.compute-1.amazonaws.com',
            database: 'dk1uaqg55fd8h',
            password: '5419f1ad52afc09638ff4429366bd82ac606c06c37e160602e344d936193c3ea',
            port: 5432,
        };
        /*this.config = {
            user: 'postgres',
            host: 'localhost',
            database: 'planet_db',
            password: 'root',
            port: 5432,
        };*/
        this.pool = new Pool(this.config);
    }

    // Expose the pool instance to other classes.
    getPool() {
        return this.pool;
    }
}

// Export a new instance of the Database class as db
module.exports.db = new DB();