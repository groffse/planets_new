const crypto = require('crypto');

/**
 * Class Auth
 * Handle any requests that require authentication.
 */
class Auth {

    /**
     * @param {Object} pool 
     */
    constructor(pool) {
        this.pool = pool;
    }

    /**
     * Check the Request for an Authorization header.
     * The header should contain the api_key
     */
    async authenticate(req, resp) {
        try {
            const { authorization } = req.headers;

            if (!authorization) {
                return {
                    error: 'No API key found.'
                };
            }

            const key = authorization.split(' ')[1];
            const keyResult = await this.pool.query('SELECT * FROM api_key WHERE key = $1 AND active = 1', [key]);

            if (keyResult.rowCount > 0) {
                return true;
            } else {
                return {
                    error: 'Invalid api key'
                };
            }
        }
        catch (e) {
            return {
                error: e.message
            }
        }
    }

    /**
     * Generate a new API Key for our client
     * This path should NOT require authentication.
     * @param {*} req 
     * @param {*} resp 
     */
    async generateApiKey(req, resp) {
        try {
            // Use the node crypto package to generate a random 32 character string in HEX format.
            const apiKey = await crypto.randomBytes(32).toString('hex');
            // Insert the new api_key into the api_key table.
            const result = await this.pool.query('INSERT INTO api_key (key) VALUES($1) RETURNING ID', [apiKey]);
            if (result.rowCount > 0) {
                resp.json({
                    apiKey
                });
            } else {
                resp.status(500).json({
                    success: false,
                    message: 'Could not create a new API Key'
                });
            }            
        }
        catch(e) {
            resp.json({
                error: e.message
            })
        }
    }

}

module.exports.Auth = Auth;